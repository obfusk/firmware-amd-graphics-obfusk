## Building & Installing

### Cloning (first time)

```bash
$ git clone https://salsa.debian.org/obfusk/firmware-amd-graphics-obfusk.git
$ cd firmware-amd-graphics-obfusk
$ git submodule init
$ git submodule update
```

### Updating (afterwards)

```bash
$ cd firmware-amd-graphics-obfusk
$ git pull
$ git submodule update
$ dpkg-buildpackage -Tclean
```

### Verifying (optional)

```bash
$ cd firmware-amd-graphics-obfusk
$ gpg --import debian/upstream/signing-key.asc    # only needed once
$ cd linux-firmware
$ git verify-tag "$( git describe )"
```

### Building

```bash
$ cd firmware-amd-graphics-obfusk
$ dpkg-buildpackage -A
```

### Installing

NB: adjust filename accordingly.

```bash
$ sudo dpkg -i firmware-amd-graphics-obfusk_20220913-1_all.deb
```

## Updating to New Upstream Releases

```bash
$ cd firmware-amd-graphics-obfusk/linux-firmware
$ git fetch
$ git checkout $NEWTAG                            # e.g. 20220913
$ cd ..
$ dch -v $NEWTAG-1
$ git add -u
$ git commit -m 'new upstream version $NEWTAG'    # merge request ?!
```
