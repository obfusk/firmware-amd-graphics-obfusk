SHELL := /bin/bash

FILES := LICENSE.amdgpu LICENSE.radeon README amdgpu r128 radeon

.PHONY: all clean

all:
	for x in $(FILES); do \
	  test -e "$$x" || cp -a linux-firmware/"$$x" ./ ; \
	done

clean:
	rm -fr $(FILES)
